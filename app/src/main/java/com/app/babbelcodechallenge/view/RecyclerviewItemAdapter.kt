package com.app.babbelcodechallenge.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.babbelcodechallenge.R
import com.app.babbelcodechallenge.data.LeaderBoard

class RecyclerviewItemAdapter internal constructor(private val itemsList: List<LeaderBoard>) :
    RecyclerView.Adapter<RecyclerviewItemAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = itemsList[position]
        holder.tvRight.text = "Correct Points: " + item.correct.toString()
        holder.tvWrong.text = "Wrong Points: " + item.wrong.toString()
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvRight: TextView = itemView.findViewById(R.id.tvRight)
        var tvWrong: TextView = itemView.findViewById(R.id.tvWrong)
    }
}