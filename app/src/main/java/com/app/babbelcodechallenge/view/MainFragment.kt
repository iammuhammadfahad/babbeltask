package com.app.babbelcodechallenge.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.app.babbelcodechallenge.R

class MainFragment : Fragment() {

    lateinit var btnStart: Button
    lateinit var btnLeaderBoard: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnStart = view.findViewById(R.id.btn_start)
        btnLeaderBoard = view.findViewById(R.id.btn_history)
        btnStart.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.add((requireView().parent as ViewGroup).id, GameFragment())
                ?.commit()
        }

        btnLeaderBoard.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.add((requireView().parent as ViewGroup).id, LeaderboardFragment())
                ?.commit()
        }
    }
}