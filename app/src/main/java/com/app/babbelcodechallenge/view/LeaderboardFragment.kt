package com.app.babbelcodechallenge.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.babbelcodechallenge.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LeaderboardFragment : Fragment() {
    private lateinit var model: LeaderBoardViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_leaderboard, container, false)
        CoroutineScope(Dispatchers.IO).launch {
            model = LeaderBoardViewModel(requireActivity().application)
            val recyclerView = view.findViewById<View>(R.id.recycleView) as RecyclerView
            val recyclerviewItemAdapter = RecyclerviewItemAdapter(model.allStudents)
            recyclerView.setHasFixedSize(true)
            val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
            recyclerView.layoutManager = layoutManager
            recyclerView.itemAnimator = DefaultItemAnimator()
            recyclerView.adapter = recyclerviewItemAdapter
        }
        return view
    }
}