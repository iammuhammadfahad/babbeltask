package com.app.babbelcodechallenge.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.app.babbelcodechallenge.R
import com.app.babbelcodechallenge.data.LeaderBoard
import com.app.babbelcodechallenge.model.GameData
import com.app.babbelcodechallenge.utitls.GetDataFromJson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class GameFragment : Fragment() {

    private var fallingCount: Int = 0
    private lateinit var testModel: GameData
    lateinit var tvRightCount: TextView
    lateinit var tvWrongCount: TextView
    lateinit var displayWord: TextView
    lateinit var fallingWord: TextView
    lateinit var wrongButton: Button
    lateinit var correctButton: Button
    lateinit var quitButton: Button
    private var count = 0
    private var rightCount = 0
    private var wrongCount = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_game, container, false)
        displayWord = view.findViewById(R.id.displayWord)
        fallingWord = view.findViewById(R.id.fallingWord)
        wrongButton = view.findViewById(R.id.wrongButton)
        correctButton = view.findViewById(R.id.correctButton)
        quitButton = view.findViewById(R.id.quitButton)
        tvRightCount = view.findViewById(R.id.tvRightCount)
        tvWrongCount = view.findViewById(R.id.tvWrongCount)

        val getData = GetDataFromJson()
        testModel = context?.let { getData.getData(it) }!!
        callGame(count)

        wrongButton.setOnClickListener {
            if (testModel[count].textSpa != testModel[fallingCount].textSpa) {
                ++rightCount
            } else {
                ++wrongCount
            }
            callGame(++count)
        }

        correctButton.setOnClickListener {
            if (testModel[count].textSpa == testModel[fallingCount].textSpa) {
                ++rightCount
            } else {
                ++wrongCount
            }

            callGame(++count)
        }

        quitButton.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                val model = LeaderBoardViewModel(requireActivity().application)
                val data = LeaderBoard(0, rightCount, wrongCount)
                model.insert(data)
                activity?.supportFragmentManager?.beginTransaction()?.remove(this@GameFragment)
                    ?.commit();
            }
        }

        return view
    }

    private fun callGame(count: Int) {
        tvRightCount.text = "Correct Answers: $rightCount"
        tvWrongCount.text = "Wrong Answers: $wrongCount"
        displayWord.text = (testModel[count].textEng)
        fallingCount = (count..testModel.size).random()
        fallingWord.text = (testModel[fallingCount].textSpa)
    }
}