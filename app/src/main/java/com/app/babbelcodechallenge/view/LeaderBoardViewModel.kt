package com.app.babbelcodechallenge.view

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.app.babbelcodechallenge.data.LeaderBoard
import com.app.babbelcodechallenge.data.LeaderBoardDatabase

class LeaderBoardViewModel(application: Application) : AndroidViewModel(application) {
    private val db: LeaderBoardDatabase = LeaderBoardDatabase.getInstance(application)
    internal val allStudents: List<LeaderBoard> = db.gameDao().getAll()

    fun insert(student: LeaderBoard) {
        db.gameDao().insert(student)
    }
}