package com.app.babbelcodechallenge.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [LeaderBoard::class], version = 1)
abstract class LeaderBoardDatabase : RoomDatabase() {
    abstract fun gameDao(): LeaderBoardDao

    companion object {
        private var INSTANCE: LeaderBoardDatabase? = null
        fun getInstance(context: Context): LeaderBoardDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context,
                    LeaderBoardDatabase::class.java,
                    "roomdb"
                )
                    .build()
            }

            return INSTANCE as LeaderBoardDatabase
        }
    }
}