package com.app.babbelcodechallenge.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LeaderBoard(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @ColumnInfo(name = "correct") val correct: Int?,
    @ColumnInfo(name = "wrong") val wrong: Int?
)