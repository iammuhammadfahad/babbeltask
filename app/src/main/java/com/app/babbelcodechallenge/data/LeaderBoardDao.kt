package com.app.babbelcodechallenge.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface LeaderBoardDao {
    @Query("SELECT * FROM leaderboard")
    fun getAll(): List<LeaderBoard>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(vararg LeaderBoards: LeaderBoard)

}