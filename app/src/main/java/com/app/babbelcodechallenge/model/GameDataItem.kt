package com.app.babbelcodechallenge.model

import com.google.gson.annotations.SerializedName

data class GameDataItem(
    @SerializedName("text_eng")
    val textEng: String,
    @SerializedName("text_spa")
    val textSpa: String
)