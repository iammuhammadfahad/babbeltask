package com.app.babbelcodechallenge.utitls

import android.content.Context
import com.app.babbelcodechallenge.R
import com.app.babbelcodechallenge.model.GameData
import com.google.gson.Gson
import java.io.*

class GetDataFromJson {
    fun getData(context: Context): GameData {
        val `is`: InputStream = context.resources.openRawResource(R.raw.game)
        val writer: Writer = StringWriter()
        val buffer = CharArray(1024)
        `is`.use { `is` ->
            val reader: Reader = BufferedReader(InputStreamReader(`is`, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        }
        val jsonString: String = writer.toString()
        val gson = Gson()
        return gson.fromJson(jsonString, GameData::class.java)
    }
}